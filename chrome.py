# -*- coding: iso-8859-15 -*-
import platform
import time

from selenium import webdriver

driver_path = "./bin/chromedriver_win32.exe"

if platform.system().startswith('Linux'):
    driver_path = "./bin/chromedriver_linux64"
elif platform.system().startswith('Mac') or platform.system().startswith('Darwin'):
    driver_path = './bin/chromedriver_mac32'    

driver = webdriver.Chrome(driver_path)

driver.get('http://www.gastecnologia.com/contato')

driver.find_element_by_id("nome").clear()
driver.find_element_by_id("nome").send_keys(u"Jos� da Silva")
time.sleep(1)

driver.find_element_by_css_selector("#estado-button > span.ui-selectmenu-text").click()
driver.find_element_by_id("ui-id-13").click()
time.sleep(1)

driver.find_element_by_id("mensagem").clear()
driver.find_element_by_id("mensagem").send_keys("Hello, world!")
time.sleep(5)


driver.quit()