# -*- coding: iso-8859-15 -*-
import platform
import time

from selenium import webdriver
from selenium.webdriver.chrome import service

is_mac = False

if platform.system().startswith('Linux'):
    opera_driver_path = "./bin/operadriver"
    opera_binary_path = "/usr/bin/opera"
elif platform.system().startswith('Windows'):
    opera_driver_path = "./bin/operadriver.exe"
    opera_binary_path = "C:\\Program Files (x86)\\Opera\\31.0.1889.99\\opera.exe"
elif platform.system().startswith('Mac') or platform.system().startswith('Darwin'):
    is_mac = True    
    #opera_driver_path = "/Users/edson/projetos/mundo_gasoso/bin/operadriver_mac"
    #opera_binary_path = "/Applications/Opera.app/Contents/MacOS"

if not is_mac:
    webdriver_service = service.Service(opera_driver_path)
    webdriver_service.start()

    desiredCapabilities =  {
        'operaOptions': {
            'binary': opera_binary_path
        }
    }

    driver = webdriver.Remote(webdriver_service.service_url, desiredCapabilities)
else:
    driver = webdriver.Chrome('./bin/operadriver_mac')
    
driver.get('http://www.gastecnologia.com/contato')

driver.find_element_by_css_selector("span.ui-selectmenu-text").click()
driver.find_element_by_id("ui-id-2").click()

driver.find_element_by_css_selector("#motivo_contato-button > span.ui-selectmenu-text").click()
driver.find_element_by_id("ui-id-4").click()
time.sleep(1)

driver.find_element_by_id("empressa").clear()
driver.find_element_by_id("empressa").send_keys(u"Jos� da Silva S. A.")
time.sleep(1)

driver.find_element_by_css_selector("#estado-button > span.ui-selectmenu-text").click()
driver.find_element_by_id("ui-id-13").click()
time.sleep(1)

driver.find_element_by_id("mensagem").clear()
driver.find_element_by_id("mensagem").send_keys("Hello, world!")
time.sleep(5)


driver.quit()