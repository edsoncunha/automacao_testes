# -*- coding: iso-8859-15 -*-
import time

from selenium import webdriver  

driver = webdriver.Firefox()

driver.get('http://www.gastecnologia.com/contato')

driver.find_element_by_id("nome").clear()
driver.find_element_by_id("nome").send_keys(u"Jos� da Silva")
time.sleep(1)

driver.find_element_by_css_selector("#estado-button > span.ui-selectmenu-text").click()
driver.find_element_by_id("ui-id-13").click()
time.sleep(1)

driver.find_element_by_id("mensagem").clear()
driver.find_element_by_id("mensagem").send_keys("Hello, world!")
time.sleep(5)


driver.quit()