require "json"
require "selenium-webdriver"

@driver = Selenium::WebDriver.for :safari
@base_url = "http://www.gastecnologia.com/"
@accept_next_alert = true
@driver.manage.timeouts.implicit_wait = 30
@verification_errors = []

@driver.get(@base_url + "/contato")
@driver.find_element(:id, "nome").clear
@driver.find_element(:id, "nome").send_keys "José da Silva"
  
@driver.quit
